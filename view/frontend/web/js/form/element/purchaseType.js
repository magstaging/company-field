define([
    'Magento_Ui/js/form/element/select',
    'uiRegistry'
], function (Select, registry) {
    'use strict';

    return Select.extend({
        defaults: {
            skipValidation: false
        },

        initialize: function () {
            this._super();

            this.toggleCompanyField()
        },

        onUpdate: function () {
            this._super();

            this.toggleCompanyField()
        },

        toggleCompanyField: function () {
            registry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.company', function(field){
                if (field.visible()) {
                    field.hide()
                } else {
                    field.show()
                }
            });

            // change for Jelle
            registry.get('checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset.fax', function(field){
                if (field.visible()) {
                    field.hide()
                } else {
                    field.show()
                }
            });
        }
    })
});