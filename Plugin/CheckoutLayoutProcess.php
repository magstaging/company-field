<?php
/**
 * Created by PhpStorm.
 * User: hervetribouilloy
 * Date: 24/02/2019
 * Time: 17:04
 */

namespace Mbs\CompanyField\Plugin;

use Magento\Checkout\Block\Checkout\LayoutProcessor;
use Magento\Framework\App\Config\ScopeConfigInterface;

class CheckoutLayoutProcess
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function afterProcess(LayoutProcessor $subject, $jsLayout)
    {
        //check for C2Q
        $shippingConfig = &$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress'];
        if (isset($shippingConfig['template']) && ($shippingConfig['template'] == 'Cart2Quote_Quotation/quote-checkout/shipping')) {
            return $jsLayout;
        }

        $customAttributeCode = 'purchase_type';
        $customField = [
            'component' => 'Mbs_CompanyField/js/form/element/purchaseType',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/checkbox-set',
                'id' => 'drop-down'
            ],
            'dataScope' => 'shippingAddress.custom_attributes.purchase_type',
            'label' => 'Type bestelling', // 'label' => 'Purchase Type', should have used translation feature instead
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => ['required-entry' => true],
            'sortOrder' => 1,
            'id' => 'drop-down',
            'options' => [
                [
                    'value' => 'particulier',
                    'label' => 'Particulier',
                ],
                [
                    'value' => 'zakelijk',
                    'label' => 'Zakelijk',
                ]
            ]
        ];

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$customAttributeCode] = $customField;

        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
                    ['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['prefix']['config']['elementTmpl'] = "ui/form/element/checkbox-set";

        $customAttributeCode = 'purchase_billing_type';

        if ($this->isBillingOnPaymentPage()) {
            $customField = [
                'component' => 'Mbs_CompanyField/js/form/element/purchaseBillingType',
                'config' => [
                    'customScope' => 'billingAddress.custom_attributes',
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'ui/form/element/checkbox-set',
                    'id' => 'drop-down',
                    'pathBillingFormFields' => 'checkout.steps.billing-step.payment.afterMethods.billing-address-form.form-fields'
                ],
                'dataScope' => 'billingAddress.custom_attributes.purchase_billing_type',
                'label' => 'Type bestelling', // 'label' => 'Purchase Type', should have used translation feature instead
                'provider' => 'checkoutProvider',
                'visible' => true,
                'validation' => ['required-entry' => true],
                'sortOrder' => 1,
                'id' => 'drop-down',
                'options' => [
                    [
                        'value' => 'particulier',
                        'label' => 'Particulier',
                    ],
                    [
                        'value' => 'zakelijk',
                        'label' => 'Zakelijk',
                    ]
                ]
            ];

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']
                ['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children'][$customAttributeCode] = $customField;

            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']
                ['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']['children']['prefix']['config']['elementTmpl'] = "ui/form/element/checkbox-set";

        } else {
            $configuration = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'];
            foreach ($configuration as $paymentGroup => $groupConfig) {
                if (isset($groupConfig['component']) and $groupConfig['component'] === 'Magento_Checkout/js/view/billing-address') {
                    $customField = [
                        'component' => 'Mbs_CompanyField/js/form/element/purchaseBillingType',
                        'config' => [
                            'customScope' => 'billingAddress.custom_attributes',
                            'template' => 'ui/form/field',
                            'elementTmpl' => 'ui/form/element/checkbox-set',
                            'id' => 'drop-down',
                            'paymentGroup' => $paymentGroup,
                            'pathBillingFormFields' => 'checkout.steps.billing-step.payment.payments-list.' . $paymentGroup . '.form-fields'
                        ],
                        'dataScope' => 'billingAddress.custom_attributes.purchase_type',
                        'label' => 'Type bestelling', // 'label' => 'Purchase Type', should have used translation feature instead
                        'provider' => 'checkoutProvider',
                        'visible' => true,
                        'validation' => ['required-entry' => true],
                        'sortOrder' => 1,
                        'id' => 'drop-down',
                        'options' => [
                            [
                                'value' => 'particulier',
                                'label' => 'Particulier',
                            ],
                            [
                                'value' => 'zakelijk',
                                'label' => 'Zakelijk',
                            ]
                        ]
                    ];

                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children'][$customAttributeCode] = $customField;

                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                        ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['prefix']['config']['elementTmpl'] = "ui/form/element/checkbox-set";

                }
            }
        }

        return $jsLayout;
    }

    private function isBillingOnPaymentPage()
    {
        return $this->scopeConfig->getValue('checkout/options/display_billing_address_on');
    }
}
