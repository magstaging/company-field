# Company Field

This module adds a select field in the checkout and the latter uses a knockout model that handles the logic for the extra field 
   
1. clone the repository

3. create folder app/code/Mbs/CompanyField when located at the root of the Magento site

4. copy the content of this repository within the folder

5. install the module php bin/magento setup:upgrade

6. go to checkout and we should see a new field called Purchase Type and the select triggers the company field to appear/disappear